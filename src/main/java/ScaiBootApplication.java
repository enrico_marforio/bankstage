package it.scai.boot;

import org.springframework.boot.Banner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@SpringBootApplication
public class ScaiBootApplication {
	
	public static void main(String[] args) {
	    SpringApplication app = new SpringApplication(ScaiBootApplication.class);
	    app.setBannerMode(Banner.Mode.OFF);
	    app.run(args);
	  }

}
